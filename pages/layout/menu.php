<ul class="menu">
    <li class="sidebar-title">Menu</li>
    <li class="sidebar-item <?php echo ($page == "users" ? "active" : "")?>">
        <a href="<?php echo ($page == "users" ? "index.php" : "../users")?>" class='sidebar-link'>
            <i class="bi bi-people"></i>
            <span>Users</span>
        </a>
    </li>
    <li class="sidebar-item <?php echo ($page == "populate_details" ? "active" : "")?>">
        <a href="<?php echo ($page == "populate_details" ? "index.php" : "../populate_details")?>" class='sidebar-link'>
            <i class="bi bi-grid-fill"></i>
            <span>Populate Details</span>
        </a>
    </li>
    <li class="sidebar-item <?php echo ($page == "searching" ? "active" : "")?>">
        <a href="<?php echo ($page == "searching" ? "index.php" : "../searching")?>" class='sidebar-link'>
            <i class="bi bi-search"></i>
            <span>Searching</span>
        </a>
    </li>
    <li class="sidebar-item <?php echo ($page == "brwosing" ? "active" : "")?>">
        <a href="#" class='sidebar-link'>
            <i class="bi bi-grid-1x2-fill"></i>
            <span>Browsing</span>
        </a>
    </li>
    <li class="sidebar-item  ">
        <a href="../../apps/logout.php" class="sidebar-link">
            <i class="bi bi-box-arrow-right"></i>
             <span>Logout</span>
        </a>
    </li>
</ul>
